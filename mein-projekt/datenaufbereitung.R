# Datenaufbereitung

# Pakete laden --------------------------------------------------------------------------------

library(readr)
library(dplyr)

# Eigene Funktionen laden ---------------------------------------------------------------------

source(file = "mein-projekt/funktionen/anzahl-kino-namen.R")

# Datenobjekte laden --------------------------------------------------------------------------

df_kinos_aufbereitet <- read_rds(file = "mein-projekt/gespeicherte-objekte/df-kinos.rds")

# Fehlende Werte beseitigen -------------------------------------------------------------------

df_kinos_aufbereitet

df_kinos_aufbereitet_keine_fehlende_werte <-
    df_kinos_aufbereitet |>
    na.omit()
df_kinos_aufbereitet_keine_fehlende_werte

# View(df_kinos_aufbereitet_keine_fehlende_werte)

# Daten auswählen und umbennen ----------------------------------------------------------------
# Miniausblick auf dplyr …

df_kinos_aufbereitet_keine_fehlende_werte |>
    colnames()

# Base R Syntax "modern"
df_kinos_aufbereitet_keine_fehlende_werte |>
    rename(
        "X" = X_LV95,
        "Y" = Y_LV95
    ) |>
    select(X, Y)

# Base R Syntax "klassisch"
select(
    .data = rename(
        .data = df_kinos_aufbereitet_keine_fehlende_werte,
        "X" = X_LV95,
        "Y" = Y_LV95
    ),
    X,
    Y
)

df_kinos_aufbereitet_keine_fehlende_werte |> View()

# Optimierte Auswahl und Umbenennung in einem
df_kinos_anzahl <-
    df_kinos_aufbereitet_keine_fehlende_werte |>
    select(
        "name" = Kinonamen,
        "name_historisch" = Kinonamen_history
    ) |>
    count(name, name_historisch) |>
    arrange(desc(n))
df_kinos_anzahl

# Ziel: Tibble mit Namen und Anzahl.
# Subziel: Parameter zur Auswahl aktueller Namen oder historischer Namen.
df_kinos_aufbereitet_keine_fehlende_werte |>
    anzahl_kino_name(
        historische_namen = FALSE,
        filter_anzahl = 1
    )


df_kinos_aufbereitet_keine_fehlende_werte |>
    anzahl_kino_name(
        historische_namen = FALSE,
        filter_anzahl = 2
    )

df_3 <-
    df_kinos_aufbereitet_keine_fehlende_werte |>
        anzahl_kino_name(
            historische_namen = TRUE,
            filter_anzahl = 2
        )
df_3

# Daten für eine Visualisierung aufbereiten
df_visualisierung <-
    df_kinos_aufbereitet_keine_fehlende_werte |>
    anzahl_kino_name(
        historische_namen = TRUE
    )
df_visualisierung


# Aufbereitete Daten als R Objekt speichern ---------------------------------------------------

write_rds(
    x = df_kinos_aufbereitet_keine_fehlende_werte,
    file = "mein-projekt/gespeicherte-objekte/df-kinos-ohne-na.rds"
)

write_rds(
    x = df_kinos_anzahl,
    file = "mein-projekt/gespeicherte-objekte/df-kinos-anzahl.rds"
)

write_rds(
    x = df_visualisierung,
    file = "mein-projekt/gespeicherte-objekte/df-visualisierung.rds"
)
